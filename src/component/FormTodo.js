import React, { Component } from "react";
import { Button, Form } from "react-bootstrap";

export default class FormTodo extends Component {
  render() {
    return (
      <>
        <Form className="my-3">
          <Form.Group className="mb-3">
            <Form.Label className="fs-3 text-danger fw-bold">
              Enter your Task
            </Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter your task"
              onChange={(e) => this.props.onChange(e.target.value)}
            />
          </Form.Group>
          <Button variant="primary" onClick={() => this.props.onSubmit()}>
            Submit
          </Button>
        </Form>
      </>
    );
  }
}
