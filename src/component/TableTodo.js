import React, { Component } from "react";
import { Button, Table } from "react-bootstrap";

export default class TableTodo extends Component {
  render() {
    const data = this.props.data;
    return (
      <Table striped bordered hover variant="dark" className="text-center">
        <thead>
          <tr>
            <th>#</th>
            <th>Task</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.task}</td>
              <td>
                {
                  <Button onClick={()=>this.props.onClick(item.id)}
                    variant={item.status === "pending" ? "warning" : "success"}
                  >
                    {item.status === "pending" ? "pending" : "done"}
                  </Button>
                }
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }
}
