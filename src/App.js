import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import FormTodo from "./component/FormTodo";
import TableTodo from "./component/TableTodo";
import { Container } from "react-bootstrap";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {
          id: 1,
          task: "JAVA ",
          status: "pending",
        },
        {
          id: 2,
          task: "WEB",
          status: "pending",
        },
        {
          id: 3,
          task: "Database ",
          status: "pending",
        },
      ],
      text: "",
    };
  }

  onChange = (text) => {
    this.setState({
      text: text,
    });
  };

  onSubmit = () => {
    const { data } = this.state;
    const newObj = {
      id: data.at(-1).id + 1,
      task: this.state.text,
      status: "pending",
    };
    data.push(newObj);
    this.setState({
      data: data,
    });
  };

  onClick = (id) => {
    this.setState((pre) => {
      return {
        data: pre.data.map((item) =>
          item.id === id
            ? {
                ...item,
                status: item.status === "pending" ? "done" : "pending",
              }
            : item,
        ),
      };
    });
  };
  render() {
    return (
      <>
        <Container>
          <FormTodo onChange={this.onChange} onSubmit={this.onSubmit} />
          <TableTodo onClick={this.onClick} data={this.state.data} />
        </Container>
      </>
    );
  }
}
